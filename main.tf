provider "aws" {
  region = "ap-south-1"
}
resource "aws_instance" "terra1" {
ami = "ami-02e60be79e78fef21"
instance_type = "t2.micro"
subnet_id = "subnet-e3b02aaf"
iam_instance_profile = "SSM_ROLE"
user_data = <<-EOT
             #!/bin/bash
             sudo yum install -y https://s3.amazonaws.com/ec2-downloads-windows/SSMAgent/latest/linux_amd64/amazon-ssm-agent.rpm
             sudo systemctl start amazon-ssm-agent
EOT
tags = {
  Name = "terra1"
}
}

resource "aws_db_instance" "myrda" {
  identifier             = "database-3"
  allocated_storage      = 10
  engine                 = "MYSQL"
  engine_version         = "5.7.26"
  instance_class         = "db.t2.micro"
  name                   = "mydb"
  username               = "admin"
  password               = "nopassword"
}
terraform {
 backend "s3" {
  bucket = "s3vamshi"
  key = "rds.tfstate"
  region = "ap-south-1"
  }
}